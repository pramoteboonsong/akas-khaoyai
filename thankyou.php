<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Prompt:400,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
    <style>
        html {
            font-size: 62.5%;
        }

        body {
            margin: 0;
        }

        .thankyou {
            background: rgba(0, 0, 0, 0.5) url(assets/images/bg-thankyou.jpg);
            background-size: cover;
            height: 100vh;
            background-attachment: fixed;
            background-position: center;
        }

        h1 {
            font-family: "Montserrat", sans-serif;
            font-size: 3.6rem;
            letter-spacing: 1.33px;
            color: #262626;
            line-height: 1.3;
            margin: 0;
        }

        p {
            color: #262626;
            font-family: "Prompt", "sans-serif";
            font-size: 1.6rem;
            font-weight: normal;
            line-height: normal;
            padding: 0;
            margin: 0;

        }

        .block-content {
            position: absolute;
            height: 100vh;
            top: 0;
            bottom: 0;
            right: 5.5%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .content {
            text-align: left;
            color: #FFF;
            z-index: 2;
            width: 63.2%;
            height: auto;
            background: #fff;
            padding: 4.8rem 10rem;
        }

        .detail {
            padding: 2rem 0 11.2rem;
        }

        .content a {
            font-family: "Prompt", "sans-serif";
            font-size: 1.6rem;
            width: auto;
            background-color: #323e48;
            letter-spacing: 3px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
            padding: 3rem 5rem;
            text-transform: uppercase;
            text-decoration: none;
            display: block;
            transition: all 0.75s ease;
            -webkit-transition: all 0.75s ease;
        }

        .content a:hover {
            background: #273037;
        }
        .mobile {
                display: none;
            }
        @media only screen and (max-width: 1023px) {
            .mobile {
                display: block;
            }

            img {
                width: 100%;
                max-width: 100%;
            }

            .thankyou {
                background: none;
                height: auto;
            }

            .block-content {
                position: static;
                height: auto;
                padding: 2rem;
            }

            .content {
                padding: 0;
                width: 100%;
            }
            .content a {
                padding: 2.6rem 5rem;
            }
        }

        /* .cover {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            z-index: 1;
            background: rgba(0, 0, 0, 0.3);
        } */
    </style>
</head>

<body>
    <div class="thankyou">
        <div class="cover"></div>
        <img class="mobile" src="./assets/images/bg-thankyou.jpg" alt="">
        <div class="block-content">
            <div class="content">
                <h1>Thank you!</h1>
                <div class="detail">
                    <p>Your request has been sent. We will contact you shortly.</p>
                    <br>
                    <br>
                    <p> For further information call: 091 742 3636</p>
                    <p> Email : akascondo@gmail.com</p>
                    </p>
                </div>
                <a href="./index.php">Back to Home</a>
            </div>
        </div>
    </div>
</body>

</html>