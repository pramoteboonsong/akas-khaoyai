<?php include 'header.php'; ?>
<div class="project-building bg-cream padding-left-80 margin-menu-M height-100">
    <div class="menu-project-building d-flex justify-content-between">
        <div class="menu-proect-detail d-flex align-items-center padding-menu">
            <a class="" href="./index.php">HOME</a>
            <i class="flaticon-right-arrow"></i>
            <a class="hide-in-mobile" href="./our-project.php">PROJECTS</a>
            <!-- ---------------------------------------------------------Breadcrumb-------------------------------------- -->
            <span class="mobile">
                <div class="dropdown show">
                    <a class="" href="#" role="button" onclick="onSelectBreadcrumb()">
                        ...
                    </a>
                    <div class="dropdown-menu" id="showBreadcrumb">
                        <a class="dropdown-item" href="./our-project.php">PROJECT</a>
                        <a class="dropdown-item" href="./project-villa.php">VILLA</a>
                    </div>
                </div>
            </span>
            <!-- ---------------------------------------------------------Breadcrumb-------------------------------------- -->
            <i class="flaticon-right-arrow desktop"></i>
            <a class="desktop" href="./project-villa.php">VILLA</a>
            <i class="flaticon-right-arrow"></i>
            <a href="">BUILDING B</a>
        </div>
    </div>
    <nav class="block-floor">
        <div class="menu-floor ralative-block" id="menuFloor" onclick="onSelectFloor()"><i class="flaticon-floor"></i></div>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="btn-transparent nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" data-floor="floorSlider1" role="tab" aria-controls="nav-home" aria-selected="true">1ST FLOOR</a>
            <a class="btn-transparent nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" data-floor="floorSlider2" role="tab" aria-controls="nav-profile" aria-selected="false">2ND FLOOR</a>
        </div>
    </nav>
    <div class="item-type-block d-flex justify-content-between">
        <div class="bg-brown bg-color"></div>
        <div class="tab-content" id="nav-tabContent">
            <div class="item-type d-flex justify-content-between tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="owl-carousel owl-theme floor-slider" id="floorSlider1">
                    <div class="item">
                        <a href="./project-type-name.php" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="item-type d-flex justify-content-between tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="owl-carousel owl-theme floor-slider" id="floorSlider2" data-floor="floor2">
                    <div class="item">
                        <a href="./project-type-name.php" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME B</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME B</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="" class="">
                            <img src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                            <h3 class="text-center">TYPE NAME A</h3>
                            <h4 class="text-center">B1A - 48 sq.m.</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>