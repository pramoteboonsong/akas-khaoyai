<?php include 'header.php'; ?>
<div class="news-detail padding-left-80 height-100">
    <div class="container-fluid">
        <div class="menu-proect-detail d-flex align-items-center padding-menu">
            <a class="" href="./index.php">HOME</a>
            <i class="flaticon-right-arrow"></i>
            <a href="./news_promotion.php">News&Promotion</a>
            <i class="flaticon-right-arrow"></i>
            <a href="">Lorem Ipsum</a>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 padding-news">
                <img class="img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
            </div>
            <div class="col-md-6 col-sm-12 padding-news">

                <h3 class="text-left">Name</h3>
                <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                    auctor
                    iaculis. Fusce laoreet dapibus luctus. </p>
                <h4 class="date text-left">DD/MM/YYYY</h4>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>