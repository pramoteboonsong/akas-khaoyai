<?php include 'header.php'; ?>
<div class="contact-us">
    <div class="container-fluid padding-left-80 desktop">
        <div class="row">
            <div class="col-lg-8 col-sm-12 no-padding">
                <img class="object-fit" src="assets/images/banner.jpg" alt="home" srcset="">
            </div>
            <div class="col-lg-4 hidden-xs no-padding ">
                <div class="form-bg"></div>
            </div>
        </div>
    </div>
    <form action="" id="formRegister" class="form-register">
        <div class="block-form">
            <h2 class="">Any further question
                enquiry us</h2>
            <input type="text" placeholder="FULL NAME" name="fullname" required>
            <input type="tel" placeholder="TEL" name="tel" required>
            <input type="email" placeholder="EMAIL" name="email" required>
            <textarea placeholder="MESSAGE" name="" id="" cols="30" rows="10"></textarea>
            <button type="submit" class="btn-default bg-grey btn-register2">SUBMIT</button>
    </form>
</div>
<?php include 'footer.php'; ?>