<?php include 'header.php'; ?>
<div class="gallery-cate padding-left-80">
    <a href="./gallery.php" class="close"><i class="flaticon-back"></i>Back</a>
    <div class="block-pano">
        <input id="state" type="hidden" value="Day">
        <div id="panoDay" class="pano-item show">
            <figure class="desktop" style="background-image: url('assets/images/day.jpg');"></figure>
            <div class="scroll">
                <img class="mobile" src="./assets/images/day.jpg"></figure>
            </div>
        </div>
        <div id="panoNight" class="pano-item">
            <figure style="background-image: url('assets/images/night.jpg');"></figure>
            <div class="scroll">
                <img class="mobile" src="./assets/images/night.jpg"></figure>
            </div>
        </div>
        <div class="block-control">
            <h1 class="">Panorama</h1>
            <div class="menu-floor ralative-block " onclick="onSelectTime()"><i class="flaticon-day"></i></div>
            <div class="nav nav-tabs">
                <a class="btn-transparent nav-item nav-link select-time active" id="Day"><i class="flaticon-day"></i>Day</a>
                <a class="btn-transparent nav-item nav-link select-time" id="Night"><i class="flaticon-night"></i>Night</a>
            </div>
        </div>
    </div>

    <!-- <nav class="block-floor">
        <div class="menu-floor ralative-block" id="menuFloor" onclick="onSelectFloor()"><i class="flaticon-floor"></i></div>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="btn-transparent nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" data-floor="floorSlider1" role="tab" aria-controls="nav-home" aria-selected="true">1ST FLOOR</a>
            <a class="btn-transparent nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" data-floor="floorSlider2" role="tab" aria-controls="nav-profile" aria-selected="false">2ND FLOOR</a>
        </div>
    </nav> -->
</div>
<?php include 'footer.php'; ?>