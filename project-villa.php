<?php include 'header.php'; ?>
<div class="project-type bg-brown height-100 padding-left-80">
<div class="menu-project-building d-flex justify-content-between">
        <div class="menu-proect-detail d-flex align-items-center padding-menu">
            <a class="" href="./index.php">HOME</a>
            <i class="flaticon-right-arrow"></i>
            <a class="" href="./our-project.php">PROJECTS</a>
            <i class="flaticon-right-arrow"></i>
            <a class="" href="">VILLA</a>
        </div>
    </div>
    <div class="type-image">
        <img src="./assets/images/floorplan.jpg" alt="">
    </div>
    <div class="menu-building d-flex justify-content-between">
        <a href="./project-building.php" class="btn-transparent font-weight-sem-bold"><strong>building a</strong></a>
        <a href="./project-building.php" class="btn-transparent font-weight-sem-bold"><strong>building b</strong></a>
        <a href="./project-building.php" class="btn-transparent font-weight-sem-bold"><strong>building c</strong></a>
    </div>
</div>

<?php include 'footer.php'; ?>