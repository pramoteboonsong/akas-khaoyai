<?php include 'header.php'; ?>
<div class="gallery-page">
    <div class="block-gallery">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-12 order-md-1 no-padding position-lt">
                    <a href="./gallery-exterior.php">
                        <div class="cover-img">
                            <img class="img-fluid" src="assets/images/g1.jpg" alt="Bedroom1" srcset="">
                            <h3>Exterior</h3>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-12 order-md-3 no-padding position-center">
                    <a href="./gallery-facility.php">
                        <div class="cover-img">
                            <img img-show="myImg1" class="img-fluid" src="assets/images/g2.jpg" alt="Bedroom2" srcset="">
                            <h3>Facility</h3>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-12 order-md-4 no-padding position-rt">
                    <a href="./gallery-animation.php">
                        <div class="cover-img">
                            <img img-show="myImg2" class="img-fluid" src="assets/images/g3.jpg" alt="Bedroom3" srcset="">
                            <h3>3D Animation</h3>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-12 order-md-2 no-padding position-lb">
                    <a href="./gallery-interior.php">
                        <div class="cover-img">
                            <img img-show="myImg3" class="img-fluid" src="assets/images/g4.jpg" alt="Bedroom4" srcset="">
                            <h3>Interior</h3>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-12 order-md-5 no-padding position-rb">
                    <a href="./gallery-panorama.php">
                        <div class="cover-img">
                            <img img-show="myImg4" class="img-fluid" src="assets/images/g5.jpg" alt="Bedroom5" srcset="">
                            <h3>Panorama</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div id="galleryModal" class="modal padding-left-80">
    <span class="close"><i class="flaticon-back"></i>Back</span>
    <div class="owl-carousel owl-theme" id="myImg">
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom1" srcset="">
            <h1 class="caption">Bedroom1</h1>
        </div>
        <div class="item">
            <img img-show="myImg2" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom2" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom3" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom4" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
    </div>
   <img class="modal-content" id="img01">
<div id="galleryCaption"></div>
</div>-->
<?php include 'footer.php'; ?>