<?php include 'header.php'; ?>
<div class="news padding-left-80 ">
    <div class="container-fluid">
        <h2 class="padding-news">News & Promotion</h2>
        <div class="row">
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
            <div class="col-sm-4 padding-news">
                <a href="./news_promotion_detail.php">
                    <div class="cover-img">
                        <img class="mask img-fluid" src="https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg" alt="">
                    </div>
                    <h3 class="text-left">Name</h3>
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar
                        auctor
                        iaculis. Fusce laoreet dapibus luctus. </p>
                    <h4 class="date text-left">DD/MM/YYYY</h4>
                </a>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>