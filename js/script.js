$(document).ready(function () {

    var mouseX = 0;
    var hoveringFigure = false;
    var threshold = 400;
    var maxSpeed = 40;
    var loopSpeed = 40;
    var moveBy;
    var moveDirection = 0;
    var $panoList = $(`#panoDay figure`);
    var figureWidth = $panoList.width();

    if (window.matchMedia('(min-width: 1024px)').matches) {
        $(window).resize(function () {
            figureWidth = $panoList.width();
        });
        $panoList.mousemove(function (event) {
            mouseX = event.pageX;
            if (mouseX < threshold) {
                moveDirection = 0;
                moveBy = ((threshold - mouseX) / threshold) * maxSpeed;
            } else if (mouseX > (figureWidth - threshold)) {
                moveDirection = 1;
                moveBy = ((figureWidth - threshold - mouseX) / threshold) * maxSpeed * -1;
            } else {
                moveBy = 0;
            }
        });
        function loop() {
            if (true === hoveringFigure) {
                if (0 === moveDirection) {
                    $panoList.animate({
                        'background-position-x': '+=' + moveBy
                    }, loopSpeed, function () {
                        loop();
                    });
                } else {
                    $panoList.animate({
                        'background-position-x': '-=' + moveBy
                    }, loopSpeed, function () {
                        loop();
                    });
                }
            }
        }

        $panoList.hover(
            function () {
                hoveringFigure = true;
                loop();
            }, function () {
                hoveringFigure = false;
            }
        );

        loop();
    }
    // -------------------------------------------------------------------
    var owl = $('.gallery-home-slider');
    owl.owlCarousel({
        stagePadding: 200,
        margin: 5,
        responsiveClass: true,
        // nav: true,
        autoplay: true,
        // animateOut: 'fadeOut',
        smartSpeed: 3000,
        autoplayTimeout: 5000,
        loop: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 20
            },
            600: {
                items: 1,
                stagePadding: 30
            },
            1000: {
                items: 1
            }
        }
    });
    $('.gallery-slider').owlCarousel({
        lazyLoad: true,
        stagePadding: 50,
        margin: 5,
        responsiveClass: true,
        nav: true,
        loop: true,
        // autoPlay: 2500,
        // slideSpeed: 3000,
        smartSpeed: 1700,
        autoplayTimeout: 3000,
        // animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1,
                stagePadding: 0,
                margin: 0,
            },
            1023: {
                items: 1,
                stagePadding: 0,
                margin: 0,
            },
            1024: {
                items: 1,
                stagePadding: 100
            }
        }
    });
    $('.about-slider').owlCarousel({
        items: 1,
        lazyLoad: true,
        loop: true,
        animateOut: 'fadeOut',
        nav: true,
        smartSpeed: 8000,
        autoplayTimeout: 5000,
        autoplay: true,
        autoplayHoverPause: true,
        margin: 0
    });
    $('.floor-slider').owlCarousel({
        stagePadding: 0,
        margin: 160,
        responsiveClass: true,
        nav: true,
        // loop: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 1
            },
            768: {
                items: 1,
            },
            1023: {
                items: 4,
                margin: 50
            },
            1024: {
                items: 4
            }
        }
    });
    $('.video-slider').owlCarousel({
        // autoplay: true,
        // autoplayHoverPause: true,
        loop: true,
        items: 1,
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true, afterAction: function (current) {
            current.find('video').get(0).play();
        },
        video: true,
        lazyLoad: true,
        nav: true,
        center: true,
        responsive: {
            480: {
                items: 1
            }
        }
    });
})
// --------------------------end already-----------------------------------------
var video = document.getElementById("myVideo");

$('#myBtn').hide();

$('#myVideo').click(function () {
    if (video.paused) {
        video.play();
        $('#myBtn').hide();
        // btn.innerHTML = "";
    } else {
        video.pause();
        $('#myBtn').show();

        // btn.innerHTML = "<i class='flaticon-play-button'></i>";
    }
});

function onPlayVideo() {
    if (video.paused) {
        video.play();
        $('#myBtn').hide();
        // btn.innerHTML = "";
    } else {
        video.pause();
        $('#myBtn').show();

        // btn.innerHTML = "<i class='flaticon-play-button'></i>";
    }
}
$('.item video').click(function () {
    // alert('5555')
    $('.play-video').toggleClass('hide');
    // if (video.paused) {
    //     video.play();
    //     btn.innerHTML = "<i class='flaticon-pause-button'></i>";
    // } else {
    //     video.pause();
    //     btn.innerHTML = "<i class='flaticon-play-button'></i>";
    // }
});
function onClickMenu() {
    document.getElementById("mainMenu").classList.toggle("show");
    document.getElementById("menuList").classList.toggle("show");
    document.getElementById("titleMenu").classList.toggle("show");
    document.getElementById("coverBlur").classList.toggle("show");
    var x = document.getElementById("titleMenu");
    // setTimeout(function () {
    if (x.innerHTML === "MENU") {
        x.innerHTML = "CLOSE";
    } else {
        x.innerHTML = "MENU";
    }
    // }, 2000);
}
function onClickMenu_M() {
    document.getElementById("btnMenu_M").classList.toggle("show");
    document.getElementById("menuList_M").classList.toggle("show");
}
function onClicRegister() {
    document.getElementById("formRegister").classList.toggle("show");
    document.getElementById("btnToRegister").classList.toggle("show");
}
function onClicRegisterM() {
    document.getElementById("formRegisterM").classList.toggle("show");
    document.getElementById("btnMenu_M").classList.toggle("hide");
    document.getElementById("menuList_M").classList.toggle("hide");
    var x = document.getElementById("btnRegisterM");
    // if (x.innerHTML === "REGISTER") {
    //     x.innerHTML = "REGISTER";
    // } else {
    //     x.innerHTML = "REGISTER";
    // }
}
function onSelectFloor() {
    document.getElementById("menuFloor").classList.toggle("show");
    document.getElementById("nav-tab").classList.toggle("show");
}
function onSelectBreadcrumb() {
    document.getElementById("showBreadcrumb").classList.toggle("show");
}
function togglePlayState(newState) {
    document.getElementById("iconEffect").style.animationPlayState = newState;
}
$('.block-floor-img img').click(function () {
    var getID = $(this).attr('img-show');
    var modal = document.getElementById("floorModal");
    // var captionText = document.getElementById("caption");
    var slider = getID;
    if (slider) {
        modal.style.display = "block";
        $('#' + getID).owlCarousel({
            stagePadding: 200,
            // lazyLoad: true,
            margin: 5,
            responsiveClass: true,
            nav: true,
            // animateOut: 'fadeOut',
            smartSpeed: 450,
            autoplayTimeout: 3000,
            loop: true,
            responsive: {
                0: {
                    items: 1,
                    stagePadding: 0,
                    margin: 0,
                },
                1023: {
                    items: 1,
                    stagePadding: 0,
                    margin: 0,
                },
                1024: {
                    items: 1
                }
            }
        });
        // captionText.innerHTML = this.alt;
    }
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    }
});
$('.project').mouseover(function () {
    projectId = this.id
    if (projectId) {
        $(this).find(".image-cover").addClass("actived");
        $(this).find(".text-hover").addClass("actived");
    }
});
$('.project').mouseout(function () {
    projectId = this.id
    if (projectId) {
        $(this).find(".image-cover").removeClass("actived");
        $(this).find(".text-hover").removeClass("actived");
    }
});
$('.select-time').click(function () {
    timetId = this.id
    // console.log(timetId);

    if (timetId == 'Night') {
        document.getElementById("Night").classList.add("active");
        document.getElementById("Day").classList.remove("active");
        document.getElementById("panoNight").classList.add("show");
        document.getElementById("panoDay").classList.remove("show");
    } else {
        document.getElementById("Day").classList.add("active");
        document.getElementById("Night").classList.remove("active");
        document.getElementById("panoDay").classList.add("show");
        document.getElementById("panoNight").classList.remove("show");
    }
    // $('.nav-tabs').css('opacity', '0');
    var mouseX = 0;
    var hoveringFigure = false;
    var threshold = 400;
    var maxSpeed = 40;
    var loopSpeed = 40;
    var moveBy;
    var moveDirection = 0;
    var $panoList = $(`#pano${timetId} figure`);
    var figureWidth = $panoList.width();
    $(window).resize(function () {
        figureWidth = $panoList.width();
    });

    $panoList.mousemove(function (event) {
        mouseX = event.pageX;
        if (mouseX < threshold) {
            moveDirection = 0;
            moveBy = ((threshold - mouseX) / threshold) * maxSpeed;
        } else if (mouseX > (figureWidth - threshold)) {
            moveDirection = 1;
            moveBy = ((figureWidth - threshold - mouseX) / threshold) * maxSpeed * -1;
        } else {
            moveBy = 0;
        }
    });
    function loop() {
        if (true === hoveringFigure) {
            if (0 === moveDirection) {
                $panoList.animate({
                    'background-position-x': '+=' + moveBy
                }, loopSpeed, function () {
                    loop();
                });
            } else {
                $panoList.animate({
                    'background-position-x': '-=' + moveBy
                }, loopSpeed, function () {
                    loop();
                });
            }
        }
    }

    $panoList.hover(
        function () {
            hoveringFigure = true;
            loop();
        }, function () {
            hoveringFigure = false;
        }
    );

    loop();
});
function onSelectTime() {
    $('.nav-tabs').toggleClass('show');
    console.log(this);
}