<?php include 'header.php'; ?>
<div class="about-us padding-left-80">
    <div class="owl-carousel owl-theme owl-loaded about-slider">
        <div class="owl-stage-outer">
            <div class="owl-stage">
                <div class="owl-item">
                    <div class="cover-image"></div>
                    <img class="img-fluid" src="assets/images/out-project-2.jpg" alt="out-project" srcset="">
                    <div class="project-detail">
                        <h2>Feels of Akas Khaoyai</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar auctor
                            iaculis. Fusce laoreet dapibus luctus. Nulla facilisi.
                            Sed convallis lacus et dapibus dictum. Aenean ut nulla eget diam mollis pharetra non
                            ac dui. Mauris sodales viverra sem,
                            sed ullamcorper elit ornare convallis
                        </p>
                    </div>
                </div>
                <div class="owl-item">
                <div class="cover-image"></div>
                    <img class="img-fluid" src="assets/images/out-project-1.jpg" alt="out-project" srcset="">
                    <div class="project-detail">
                        <h2>Feels of Akas Khaoyai</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar auctor
                            iaculis. Fusce laoreet dapibus luctus. Nulla facilisi.
                            Sed convallis lacus et dapibus dictum. Aenean ut nulla eget diam mollis pharetra non
                            ac dui. Mauris sodales viverra sem,
                            sed ullamcorper elit ornare convallis
                        </p>
                    </div>
                </div>
                <div class="owl-item">
                <div class="cover-image"></div>
                    <img class="img-fluid" src="assets/images/out-project-2.jpg" alt="out-project" srcset="">
                    <div class="project-detail">
                        <h2>Feels of Akas Khaoyai</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar auctor
                            iaculis. Fusce laoreet dapibus luctus. Nulla facilisi.
                            Sed convallis lacus et dapibus dictum. Aenean ut nulla eget diam mollis pharetra non
                            ac dui. Mauris sodales viverra sem,
                            sed ullamcorper elit ornare convallis
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>