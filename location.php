<?php include 'header.php'; ?>
<div class="location height-100 bg-grey-2">
    <div class="container-fluid padding-left-80 padding-right-80">
        <div class="row">
            <div class="col-lg-8 col-sm-12">
            <h2 class="mobile text-center">Meet us!</h2>
                <img class="img-fluid google-map" src="assets/images/map.png" alt="map" srcset="">
            </div>
            <div class="col-lg-4 col-sm-12 d-flex align-items-center">
                <div class="location-detail">
                    <h2 class="desktop">Meet us!</h2>
                    <p>Phaya Yen, Pak Chong District, Nakhon Ratchasima 13130, Thailand
                    </p>
                    <a class="btn-default btn-map bg-blue d-flex align-items-center justify-content-center" href="https://goo.gl/maps/qpitj8zi1vm2tKTT9" target="_blank"><i class="flaticon-google-maps"></i>Google Map</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>