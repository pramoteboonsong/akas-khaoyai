<?php include 'header.php'; ?>
<div class="out-proect d-flex padding-left-80">
    <!-- <h1 class="text-center title top-0">Our Project</h1> -->
    <div class="block-villa">
        <a href="./project-villa.php" class="project" id="villa">
            <img class="img-fluid fit-right" src="./assets/images/out-project-1.jpg" alt="out-project" srcset="">
            <div id="imageCover1" class="image-cover"></div>
            <div id="textCover1" class="text-hover">
                <h2>Villa<i class="fa fa-long-arrow-right"></i></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Morbi pulvinar auctor iaculis. Fusce laoreet dapibus luctus.
                </p>
            </div>
        </a>
    </div>
    <div class="block-condo">
        <a  href="http://www.akaskhaoyai.com/" class="project" id="condo" target="_blank">
            <img class="img-fluid fit-left" src="./assets/images/out-project-2.jpg" alt="out-project" srcset="">
            <div id="imageCover2" class="image-cover"></div>
            <div id="textCover2" class="text-hover">
                <h2>Condominium<i class="fa fa-long-arrow-right"></i></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Morbi pulvinar auctor iaculis. Fusce laoreet dapibus luctus.
                </p>
            </div>
        </a>
    </div>
</div>

<?php include 'footer.php'; ?>