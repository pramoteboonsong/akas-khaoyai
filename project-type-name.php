<?php include 'header.php'; ?>
<div class="proect-type-name bg-cream height-100 padding-left-80 margin-menu-M">
<div class="menu-project-building d-flex justify-content-between">
        <div class="menu-proect-detail d-flex align-items-center padding-menu">
        <a class="" href="./index.php">HOME</a>
            <i class="flaticon-right-arrow"></i>
            <a class="hide-in-mobile" href="./our-project.php">PROJECTS</a>
            <span class="mobile">
                <div class="dropdown show">
                    <a class="" href="#" role="button" onclick="onSelectBreadcrumb()">
                        ...
                    </a>
                    <div class="dropdown-menu" id="showBreadcrumb">
                        <a class="dropdown-item" href="./our-project.php">PROJECT</a>
                        <a class="dropdown-item" href="./project-villa.php">VILLA</a>
                        <a class="dropdown-item" href="./project-building.php">BUILDING B</a>
                    </div>
                </div>
            </span>
            <i class="flaticon-right-arrow desktop"></i>
            <a class="desktop" href="./project-building.php">VILLA</a>
            <i class="flaticon-right-arrow desktop"></i>
            <a class="desktop" href="./project-building.php">BUILDING B</a>
            <i class="flaticon-right-arrow"></i>
            <a href="">TYPE NAME</a>
        </div>
    </div>
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-7 col-sm-12 text-left type-name-detail">
                <h2 class="">TYPE NAME A</h2>
                <h3 class="no-padding">B1A - 48 sq.m.</h3>
                <h4 class="font-weight-bold text-uppercase top-60 font-spacing-3 room-info">Room info</h4>
                <h4><i class="flaticon-bedroom"></i>2 Bedroom</h4>
                <h4><i class="flaticon-bathroom"></i>2 Bathroom</h4>
                <h4><i class="flaticon-living-room"></i>1 Living room</h4>
                <div class="room-gallery top-60">
                    <h4 class="font-weight-bold text-uppercase font-spacing-3">Room Gallery</h4>
                    <div class="col-lg-6 padding-0">
                        <div class="">
                            <div class="row block-floor-img">
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg1" src="./assets/images/out-project-2.jpg" alt="Bedroom1">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg2" src="./assets/images/out-project-2.jpg" alt="Bedroom2">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg3" src="./assets/images/out-project-2.jpg" alt="Bedroom3">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg4" src="./assets/images/out-project-2.jpg" alt="Bedroom4">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg5" src="./assets/images/out-project-2.jpg" alt="Bedroom1">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg6" src="./assets/images/out-project-2.jpg" alt="Bedroom1">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg7" src="./assets/images/out-project-2.jpg" alt="Bedroom1">
                                </div>
                                <div class="col-3 padding-3-5 cover-img">
                                    <img img-show="floorImg8" src="./assets/images/out-project-2.jpg" alt="Bedroom1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-sm-12 bg-brown bg-full-height no-padding">
                <img class="img-floorplan-center" src="./assets/images/floorplan1.png" alt="" sizes="" srcset="">
                <div class="floor-block">
                    <img src="assets/images/type-name-1.png" alt="">
                    <p class="text-center">1st floor.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="floorModal" class="modal padding-left-80">
        <span class="close"><i class="flaticon-close"></i>close</span>
        <div class="owl-carousel owl-theme" id="floorImg1">
            <div class="item">
                <img img-show="floorImg1" class="img-fluid" src="assets/images/out-project-2.jpg" alt="Bedroom1" srcset="">
                <h1 class="caption">Bedroom1</h1>
            </div>
            <div class="item">
                <img img-show="floorImg2" class="img-fluid" src="assets/images/out-project-2.jpg" alt="Bedroom2" srcset="">
                <h1 class="caption">Bedroom2</h1>
            </div>
            <div class="item">
                <img img-show="floorImg3" class="img-fluid" src="assets/images/out-project-2.jpg" alt="Bedroom3" srcset="">
                <h1 class="caption">Bedroom3</h1>
            </div>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>