<!doctype html>
<html lang="en-US" class="js">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Akas Khaoyai</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="robots" content="noindex,follow">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="article:publisher" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">

    <!-- <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Prompt:400,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font/flaticon.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="scss/style.css">
</head>

<body class="">
    <header>
        <div class="menu" id="mainMenu">
            <div class="menu-group">
                <p onclick="onClickMenu()" class="title-menu desktop" id="titleMenu">MENU</p>
                <a href="http://" target="_blank" rel="noopener noreferrer"><i class="flaticon-facebook"></i></a>
                <a href="http://" target="_blank" rel="noopener noreferrer"><i class="flaticon-tel"></i></i></a>
            </div>
            <a onclick="onClicRegisterM()" href="#" id="btnRegisterM" class="btn-default btn-register mobile" rel="">REGISTER<i class="flaticon-next"></i></a>
            <div class="menu-mobile" id="btnMenu_M" onclick="onClickMenu_M()">
                <span class="top"></span>
                <span class="center"></span>
                <span class="bottom"></span>
            </div>
        </div>

        <div id="menuList_M" class="mobile">
            <div class="menu-vertical-center">
                <a href="./index.php" class="font-weight-sem-bold text-uppercase">Home</a>
                <a href="./about.php" class="font-weight-sem-bold text-uppercase">About Akas</a>
                <a href="./our-project.php" class="font-weight-sem-bold text-uppercase">Our Projects</a>
                <a href="./gallery.php" class="font-weight-sem-bold text-uppercase">Gallery</a>
                <a href="./news_promotion.php" class="font-weight-sem-bold text-uppercase">News & Promotion</a>
                <a href="./location.php" class="font-weight-sem-bold text-uppercase">Location</a>
                <a href="./contact-us.php" class="font-weight-sem-bold text-uppercase">Contact</a>
            </div>
            <div class="social">
                <a href="http://" target="_blank" rel="noopener noreferrer"><i class="flaticon-facebook"></i></a>
                <a href="http://" target="_blank" rel="noopener noreferrer"><i class="flaticon-tel"></i></i></a>
            </div>
        </div>
        <div id="menuList" class="desktop">
            <div class="menu-vertical-center">
                <a href="./index.php" class="font-weight-sem-bold text-uppercase">Home</a>
                <a href="./about.php" class="font-weight-sem-bold text-uppercase">About Akas</a>
                <a href="./our-project.php" class="font-weight-sem-bold text-uppercase">Our Projects</a>
                <a href="./gallery.php" class="font-weight-sem-bold text-uppercase">Gallery</a>
                <a href="./news_promotion.php" class="font-weight-sem-bold text-uppercase">News & Promotion</a>
                <a href="./location.php" class="font-weight-sem-bold text-uppercase">Location</a>
                <a href="./contact-us.php" class="font-weight-sem-bold text-uppercase">Contact</a>
            </div>
        </div>
        <div id="coverBlur"></div>
        <form action="" id="formRegisterM" class="form-register mobile">
            <h2>Register for more information</h2>
            <input type="text" placeholder="FULL NAME" name="fullname" required>
            <input type="tel" placeholder="TEL" name="tel" required>
            <input type="email" placeholder="EMAIL" name="email" required>
            <textarea placeholder="MESSAGE" name="message" id="" cols="30" rows="10"></textarea>
            <button type="submit" class="btn-default bg-grey btn-register2">REGISTER</button>
        </form>
    </header>