<?php include 'header.php'; ?>
<div class="gallery-cate padding-left-80">
    <a href="./gallery.php" class="close"><i class="flaticon-back"></i>Back</a>
    <div class="owl-carousel owl-theme gallery-slider">
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom1" srcset="">
            <h1 class="caption">Bedroom1</h1>
        </div>
        <div class="item">
            <img img-show="myImg2" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom2" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom3" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
        <div class="item">
            <img img-show="myImg1" class="img-fluid" src="assets/images/banner-gallery.jpg" alt="Bedroom4" srcset="">
            <h1 class="caption">Bedroom2</h1>
        </div>
    </div>
    <!-- <img class="modal-content" id="img01">-->
    <!-- <div id="galleryCaption"></div> -->
</div>
<?php include 'footer.php'; ?>