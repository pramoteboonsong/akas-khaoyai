<?php include 'header.php'; ?>
<div class="gallery-cate padding-left-80">
    <a href="./gallery.php" class="close"><i class="flaticon-back"></i>Back</a>
    <div class="block-pano">
        <div class="owl-carousel owl-theme video-slider">
            <div class="item">
                <button class="play-video" class="desktop"><i class="flaticon-play-button"></i></button>
                <video width="100vw" height="auto" controls>
                    <source src="assets/video/rain.mp4" type="video/mp4">
                    <source src="assets/video/rain.mp4" type="video/ogg">
                </video>
                <h1 class="caption">Exterior Walkthrough</h1>
            </div>
            <div class="item">
                <video width="100vw" height="auto" controls poster="assets/images/banner.jpg">
                    <source src="assets/video/rain.mp4" type="video/mp4">
                    <source src="assets/video/rain.mp4" type="video/ogg">
                </video>
                <h1 class="caption">Exterior Walkthrough</h1>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>