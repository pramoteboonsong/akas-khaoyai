<?php include 'header.php'; ?>
<div class="padding-left-80">
    <div class="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-xs-12 no-padding">
                    <video muted loop autoplay playsinline id="myVideo" class="">
                        <source src="http://akaskhaoyai.com/wp-content/uploads/2019/11/akas-HD_TS_1.mp4" type="video/mp4">
                    </video>
                    <button id="myBtn" class="desktop" onclick="onPlayVideo()"><i class="flaticon-play-button"></i></button>
                    <a onclick="onClicRegister()" href="#" id="btnToRegister" class="btn-default bg-nude btn-register desktop" rel=""><i class="flaticon-back"></i>REGISTER</a>
                </div>
            </div>
        </div>
        <form action="" id="formRegister" class="form-register desktop">
            <h2>Register for more information</h2>
            <input type="text" placeholder="FULL NAME" name="fullname" required>
            <input type="tel" placeholder="TEL" name="tel" required>
            <input type="email" placeholder="EMAIL" name="email" required>
            <textarea placeholder="MESSAGE" name="message" id="" cols="30" rows="10"></textarea>
            <button type="submit" class="btn-default bg-grey btn-register2">REGISTER</button>
        </form>
    </div>
    <div class="project-concept block-content">
        <div class="container-fluid no-padding ralative-block">
            <div class="row align-items-center ralative-block no-margin">
                <div class="col-lg-6 col-xs-12 order-M-2 M-no-padding">
                    <div class="concept-detail">
                        <h1 class="desktop">Project Concept</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pulvinar auctor iaculis.
                            Fusce
                            laoreet dapibus luctus. Nulla facilisi.
                            Sed convallis lacus et dapibus dictum. Aenean ut nulla eget diam mollis pharetra non ac
                            dui.
                            Mauris sodales viverra sem,
                            sed ullamcorper elit ornare convallis
                        </p>
                        <a onmouseover="togglePlayState('Running');" onmouseout="togglePlayState('paused');" class="btn-transparent-noborder" href="./about.php">AKAS STORY
                            <i id="iconEffect" class="flaticon-next"></i>
                        </a>
                    </div>
                    <img class="line-moutain order-M-3 desktop" src="assets/images/moutain.png" alt="" srcset="">
                </div>
                <div class="col-lg-6 col-xs-12 gap-block order-M-1">
                    <h2 class="mobile">Project Concept</h2>
                    <img class="img-fluid" src="assets/images/project-concept-img.jpg" alt="project-concept" srcset="">
                </div>
            </div>
        </div>
    </div>
    <img class="line-moutain order-M-3 mobile" src="assets/images/moutain-m.png" alt="" srcset="">
    <div class="gallery">
        <h1 class="text-center title top-0">Gallery</h1>
        <div class="container-fluid">
            <div class="row">
                <div class="ralative-block">
                    <div class="owl-carousel owl-theme gallery-home-slider">
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner-gallery.jpg" alt="out-project" srcset="">
                        </div>
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner.jpg" alt="out-project" srcset="">
                        </div>
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner-gallery.jpg" alt="out-project" srcset="">
                        </div>
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner-gallery.jpg" alt="out-project" srcset="">
                        </div>
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner-gallery.jpg" alt="out-project" srcset="">
                        </div>
                        <div class="item">
                            <img class="img-fluid" src="assets/images/banner-gallery.jpg" alt="out-project" srcset="">
                        </div>
                    </div>
                    <a href="./gallery.php" class="btn-default bg-blue btn-viewall btn-mobile" target="_blank" rel="">VIEW ALL</a>
                </div>
            </div>
        </div>
    </div>
    <div class="out-proect-home block-content nobottom M-no-padding">
        <div class="container-fluid">
            <div class="row ralative-block">
                <div class="bg-gallery">
                    <h1 class="text-center title">Our Project</h1>
                </div>
                <div class="slider ralative-block">
                    <div class="out-proect-home">
                        <!-- <h1 class="text-center title top-0">Our Project</h1> -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 gap-block">
                                    <a href="./our-project.php" class="project" id="villa">
                                        <img class="img-fluid" src="assets/images/out-project-1.jpg" alt="out-project" srcset="">
                                        <div id="imageCover1" class="image-cover"></div>
                                        <div id="textCover1" class="text-hover">
                                            <h2>Villa<i class="fa fa-long-arrow-right"></i></h2>
                                            <p>อะไรดีนะ ipsum dolor sit amet, consectetur adipiscing
                                                elit. Morbi pulvinar auctor iaculis. Fusce laoreet dapibus luctus.
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-12 gap-block">
                                    <a href="http://www.akaskhaoyai.com/" class="project" id="condo" target="_blank">
                                        <img class="img-fluid" src="assets/images/out-project-2.jpg" alt="out-project" srcset="">
                                        <div id="imageCover2" class="image-cover"></div>
                                        <div id="textCover2" class="text-hover">
                                            <h2>Condominium<i class="fa fa-long-arrow-right"></i></h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Morbi pulvinar auctor iaculis. Fusce laoreet dapibus luctus.
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-sm-12">
                    <h2 class="mobile text-center">Meet us!</h2>
                    <img class="img-fluid google-map" src="assets/images/map.png" alt="map" srcset="">
                </div>
                <div class="col-lg-4 col-sm-12 d-flex align-items-center">
                    <div class="map-detail">
                        <h2 class="desktop">Meet us!</h2>
                        <p>Phaya Yen, Pak Chong District, Nakhon Ratchasima 13130, Thailand
                        </p>
                        <a class="btn-default bg-blue d-flex align-items-center justify-content-center btn-mobile" href="https://goo.gl/maps/BDu6RxqV6LAgCJ569" target="_blank">
                            <i class="flaticon-google-maps"></i>Google Map</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>